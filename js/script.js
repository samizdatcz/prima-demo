var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

$(function () {
    $('#obce').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Velikost obcí a účast v referendu'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['< 100', '< 200', '< 400', '< 800', '< 1,6 tis.', '< 3,2 tis.', '< 6,4 tis.', '< 12,8 tis.', '< 25 tis.', '< 50 tis.', '< 100 tis.', '> 100 tis.'],
            title: {
                text: 'počet voličů v obci'
            }
        },
        yAxis: {
            title: {
                text: 'účast v referendu (%)'
            }
        },
        tooltip: {
            formatter: function() {
                if (this.x == '< 50 tis.') {
                    return ('Obce s ' + this.x + ' obyvateli: <b>' + this.y + ' %</b> účast v referendech (pouze dvě hlasování)')
                } else if (this.x == '> 100 tis.') {
                    return ('Obce s ' + this.x + ' obyvateli: <b>' + this.y + ' %</b> účast v referendech (pouze dvě hlasování)')
                } else {
                    return ('Obce s ' + this.x + ' obyvateli: <b>' + this.y + ' %</b> účast v referendech')
                }
            },
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://www.mvcr.cz/soubor/mistni-referenda-tabulka-hlaseni.aspx',
            text : 'Zdroj: Ministerstvo vnitra'
        },
        plotOptions: {
        },
        series: [{
            name: 'data',
            data: [83.0,72.1,59.8,55.1,52.2,44.3,35.2,31.7,36.5,25.2,15.6,32.7],
            color: colors[0],
            showInLegend: false
        }]
    })
})

$(function () {
    $('#svycarsko').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Švýcarsko: referenda na celostátní úrovni'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['1848–1859','1860–1869','1870–1879','1880–1889','1890–1899','1900–1909','1910–1919','1920–1929','1930–1939','1940–1949','1950–1959','1960–1969','1970–1979','1980–1989','1990–1999','2000–2009'],
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
                text: 'počet referend'
            },
            reversedStacks: false
        },
        tooltip: {
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : '',
            text : 'Zdroj: Uwe Serdült, Referendums Around the World'
        },
        plotOptions: {
        },
        series: [{
            name: 'Referenda',
            data: [1,9,11,12,22,14,11,32,23,17,45,26,86,62,100,90],
            color: colors[0],
            showInLegend: false
        }]
    })
})

})

var el = document.getElementById("obal");
el.className += " js";

function clearAll() {
	var div = el.getElementsByTagName("div");
	for (var i = 0; i < div.length; i++) {
		div[i].className = "";
	}
}

function init() {
	if (!el) return;
	var div = el.getElementsByTagName("div");

	for (var i = 0; i < div.length; i++) {
		if (div[i].parentNode != el) continue;
		div[i].getElementsByTagName("h4")[0].onclick = function () {
			var div = this.parentNode;
			var puvodni = div.className;
			clearAll();
			div.className = (div.className == "show" || puvodni == "show") ? "" : "show";
		}
	}
}

init();